// Expo Token
const token = await Notifications.getExpoPushTokenAsync();
//console.log(token)
this.setState({ expoPushToken: token });

firebase.database().ref("users").child(this.state.username).update({
  ["/expoToken"]: token
});

// Push Notif
const functions = require("firebase-functions");
var fetch = require("node-fetch");

const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

exports.sendPushNotification = functions.database
  .ref("contacts/{id}")
  .onCreate((event) => {
    const root = event.data.ref.root;
    var messages = [];

    console.log("root")
    console.log(root)

    // return the main promise
    return root
      .child("/users/user06/expoToken")
      .once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          var expoToken = childSnapshot.val().data;

          console.log(expoToken);

          if (expoToken) {
            messages.push({
              to: expoToken,
              body: "New Contact Added",
            });
          }
        }); // end of childSnapshot

        return Promise.all(messages);
      })
      .then((messages) => {
        fetch("https://exp.host/--/api/v2/push/send", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(messages),
        });
      });
  }); // end of onCreate event