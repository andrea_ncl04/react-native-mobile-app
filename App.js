import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { StatusBar } from "react-native";
import Login from "./components/login";
import SignUp from "./components/signUp";
import Profile from "./components/profile";
import GenerateQRCode from "./components/generateQRCode";
import ExpoScanner from "./components/expoScanner";
import ContactList from "./components/contactList";

class App extends React.Component {
  
  render() {

    // Navigation Variable
    const Stack = createStackNavigator();

    // Navigation Screens
    function NavStack() {
      return (
        <Stack.Navigator
          screenOptions={{
            headerTitleAlign: "center",
            headerStyle: {
              backgroundColor: "#373737",
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
            },
          }}
        >
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ title: "Mobile Test App" }}
          />
          <Stack.Screen
            name="SignUp"
            component={SignUp}
            options={{ title: "Mobile Test App" }}
          />
          <Stack.Screen
            name="Profile"
            component={Profile}
            options={{ title: "Mobile Test App" }}
          />
          <Stack.Screen
            name="GenerateQRCode"
            component={GenerateQRCode}
            options={{ title: "Mobile Test App" }}
          />
          <Stack.Screen
            name="ExpoScanner"
            component={ExpoScanner}
            options={{ title: "Mobile Test App" }}
          />
          <Stack.Screen
            name="ContactList"
            component={ContactList}
            options={{ title: "Mobile Test App" }}
          />
        </Stack.Navigator>
      );
    }

    // Return App
    return (
      <NavigationContainer>
        {/* Status Bar Styling */}
        <StatusBar backgroundColor="#494949" barStyle="light-content" />
        {/* Call NavStack Function to fetch Screens */}
        <NavStack />
      </NavigationContainer>
    );
  }
}

export default App;