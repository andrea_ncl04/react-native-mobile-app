import React from "react";
import QRCode from "react-native-qrcode-svg";
import { View, StyleSheet, Text } from "react-native";

class GenerateQRCode extends React.Component {
  constructor(props) {
    super(props);
    // Set the username to the passed parameter
    this.state = {
      username: props.route.params.username,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textHeader}>Generated QR Code:</Text>
        <QRCode value={this.state.username} />
      </View>
    );
  }
}

// UI Stylesheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#eeeeee",
  },
  textHeader: {
    marginBottom: 30,
    fontSize: 15,
    fontWeight: "bold",
  },
});

export default GenerateQRCode;
