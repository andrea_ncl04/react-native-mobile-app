import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";

class Profile extends React.Component {
  
  // States
  constructor(props) {
    super(props);

    // Set the states to the passed parameters
    this.state = {
      username: props.route.params.userName,
      firstname: props.route.params.firstName,
      lastname: props.route.params.lastName,
      gender: props.route.params.gender,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.userWrapper}>
          <Image
            style={styles.avatar}
            source={{
              uri:
                "https://www.kindpng.com/picc/m/269-2697881_computer-icons-user-clip-art-transparent-png-icon.png",
            }}
          />
          <Text style={styles.userText}> {this.state.username} </Text>
          <Text style={styles.userText}>
            {this.state.firstname} {""}
            {this.state.lastname}
          </Text>
          <Text style={styles.userText}>{this.state.gender} </Text>
        </View>

        <View style={styles.optionsWrapper}>
          <TouchableOpacity
            style={styles.btnOption}
            // Navigation and Passing Parameters
            onPress={() =>
              this.props.navigation.navigate("GenerateQRCode", {
                username: this.state.username,
              })
            }
          >
            <Text style={styles.optionText}>Generate QR Code</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnOption}
            // Navigation and Passing Parameters
            onPress={() =>
              this.props.navigation.navigate("ExpoScanner", {
                username: this.state.username,
              })
            }
          >
            <Text style={styles.optionText}>Scan QR Code</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnOption}
            // Navigation and Passing Parameters
            onPress={() =>
              this.props.navigation.navigate("ContactList", {
                username: this.state.username,
              })
            }
          >
            <Text style={styles.optionText}>View Contacts</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

// UI Stylesheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textHeader: {
    marginBottom: 30,
    fontSize: 15,
    fontWeight: "bold",
  },
  userWrapper: {
    flex: 0.6,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#8c8c8c",
  },
  userText: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 15,
  },
  avatar: {
    width: 110,
    height: 110,
    borderRadius: 63,
    borderColor: "#FFFFFF",
    marginBottom: 10,
  },
  image: {
    width: 40,
    height: 40,
  },
  optionsWrapper: {
    flex: 0.5,
    alignItems: "center",
    justifyContent: "center",
  },
  btnOption: {
    backgroundColor: "#42A5F5",
    width: "50%",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  optionText: {
    textAlign: "center",
    color: "#fff",
    fontWeight: "500",
    fontSize: 15,
  },
});

export default Profile;