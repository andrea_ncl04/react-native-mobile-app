import React from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from "react-native";
import LoadingScreen from './loadingScreen';
import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";

class Login extends React.Component {

    // Class States
    state = {
      username: "",
      password: "",
      isLoading: false,
      authorized: false,
    }
    // Parameters for Profile
    userDetails = {
      username: "",
      firstName: "",
      lastName: "",
      gender: ""
    }

  handleSignIn = () => {
    // Check if the fields are completed
    if(this.state.username === "" || this.state.password === "") {
      Alert.alert("Please provide a username and password to login.");
    } 
    else {
      // Render Loading Screen
      this.setState({ isLoading: true });

      // Check if username exists
      firebase.database().ref("users").once("value").then(snapShot => {
        snapShot.forEach(item => {
          if(this.state.username === item.key) {
            if(this.state.password === item.val().password){
              this.state.authorized = true;

              // Set the details to userDetails object
              this.userDetails.username = this.state.username;
              this.userDetails.firstName = item.val().firstName;
              this.userDetails.lastName = item.val().lastName;
              this.userDetails.gender = item.val().gender;
            }
          } 
        }) // end of for-each

        if(this.state.authorized === true) {
          Alert.alert("Logged in successfully");
          this.setState({
            isLoading: false
          })
        }
        else {
          this.setState({
            isLoading: false
          })
          Alert.alert("Login error: Please check your username and password");
        }

      }); // end of then
    } // end of if-else
  } // end of function
  
  render() {
    // Show Loading Screen if 'true'
    if(this.state.isLoading) {
      return(
        <LoadingScreen />
      );
    }
    // Show Profile Screen if authorized user
    if(this.state.authorized === true) {
      // Navigation and Passing Parameters
      this.props.navigation.navigate("Profile",{
        userName: this.userDetails.username,
        firstName: this.userDetails.firstName,
        lastName: this.userDetails.lastName,
        gender: this.userDetails.gender
      })
      // Reset states
      this.setState({
        username: "",
        password: "",
        authorized: false
      })
    }
       
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>Welcome Back!</Text>
        <TextInput 
          style={styles.textInput} 
          placeholder="Username"
          onChangeText={(username) => this.setState({ username })}
          value={this.state.username} 
        />
        <TextInput
          style={styles.textInput}
          placeholder="Password"
          secureTextEntry
          onChangeText={(password) => this.setState({ password })}
          value={this.state.password}
        />
        <TouchableOpacity
          style={styles.btnSignIn}
          onPress={() => this.handleSignIn()}
        >
          <Text style={styles.signInText}>Sign In</Text>
        </TouchableOpacity>

        <View style={styles.signUpText}>
          <Text>
            Don't have an account?{" "}
            <Text
              style={styles.btnSignUp}
              onPress={() => this.props.navigation.navigate("SignUp")}
            >
              Sign Up
            </Text>
          </Text>
        </View>
      </View>
    );
  } // end of render
} // end of class

// UI Stylesheet
const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  textInput: {
    backgroundColor: "#eeeeee",
    borderColor: "#bdbdbd",
    height: 35,
    width: "70%",
    paddingHorizontal: 10,
    color: "#333",
    marginBottom: 10,
  },
  welcomeText: {
    marginTop: 120,
    marginBottom: 30,
    fontSize: 20,
    fontWeight: "bold",
    color: "#616161",
  },
  btnSignIn: {
    backgroundColor: "#8e8e8e",
    paddingHorizontal: 103,
    paddingVertical: 10,
    borderRadius: 5,
  },
  signInText: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 15,
  },
  signUpText: {
    marginTop: 20,
  },
  btnSignUp: {
    color: "#42A5F5",
    fontWeight: "500",
    fontSize: 15,
  },
});

export default Login;