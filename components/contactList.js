import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Alert,
} from "react-native";
import "../environment/config";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";

class ContactList extends Component {
  
  constructor(props) {
    super(props);

    // States
    this.state = {
      currentUser: props.route.params.username,
      data: [],
    };
  }

  // Fetch the contacts of the currentUser from the database
  componentDidMount() {
    firebase
      .database()
      .ref("contacts/" + this.state.currentUser)
      .on("value", (snapshot) => {
        const data = [];

        snapshot.forEach((item) => {
          data.push({
            id: item.key,
            image:
              "https://img.favpng.com/18/18/14/computer-icons-message-icon-design-png-favpng-zxJRbwmFPe3JEqjaguvid4hmM.jpg",
            username: item.key,
          });
        });

        this.setState({ data });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.header}>
            <View style={styles.headerContent}>
              <Image
                style={styles.avatar}
                source={{
                  uri:
                    "https://www.kindpng.com/picc/m/269-2697881_computer-icons-user-clip-art-transparent-png-icon.png",
                }}
              />
              <Text style={styles.name}>{this.state.currentUser}</Text>
            </View>
          </View>

          <View style={styles.body}>
            <FlatList
              style={styles.container}
              enableEmptySections={true}
              data={this.state.data}
              keyExtractor={(item) => {
                return item.id;
              }}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity>
                    <View style={styles.box}>
                      <Image
                        style={styles.image}
                        source={{ uri: item.image }}
                      />
                      <Text style={styles.username}>{item.username}</Text>

                      <TouchableOpacity
                        style={styles.btnDelete}
                        onPress={() => {
                          // Delete the contact in database
                          firebase
                            .database()
                            .ref("contacts/" + this.state.currentUser)
                            .child(item.id)
                            .remove();
                          // Alert message
                          Alert.alert(
                            "User: " + item.id + " was removed from contacts."
                          );
                        }}
                      >
                        <Text style={styles.delete}>Delete</Text>
                      </TouchableOpacity>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

// UI Stylesheet
const styles = StyleSheet.create({
  header: {
    backgroundColor: "#8c8c8c",
  },
  headerContent: {
    padding: 30,
    alignItems: "center",
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderColor: "#FFFFFF",
    marginBottom: 10,
  },
  image: {
    width: 60,
    height: 60,
  },
  name: {
    fontSize: 22,
    color: "#FFFFFF",
    fontWeight: "600",
  },
  body: {
    padding: 30,
    backgroundColor: "#e6e6e6",
  },
  box: {
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 1,
      width: -2,
    },
    elevation: 2,
  },
  username: {
    color: "#20B2AA",
    fontSize: 18,
    alignSelf: "center",
    marginLeft: 10,
  },
  btnDelete: {
    alignSelf: "center",
    marginLeft: 100,
  },
  delete: {
    color: "red",
    fontSize: 12,
    alignSelf: "center",
  },
});

export default ContactList;