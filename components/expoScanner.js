import React from "react";
import { Alert, View, Text, Vibration, StyleSheet } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import * as Permissions from "expo-permissions";
import { Camera } from "expo-camera";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";

class ExpoScanner extends React.Component {
  
  constructor(props) {
    super(props);

    this.onBarCodeRead = this.onBarCodeRead.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.scannedCode = null;

    // States
    this.state = {
      currentUser: props.route.params.username,
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      validUser: false,
      contactExists: false,
    };
  }

  // Camera Permission
  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    await this.setState({ hasCameraPermission: status === "granted" });
    await this.resetScanner();
  }

  // Set "scannedItem" to the QR data
  onBarCodeRead({ type, data }) {
    if (
      (type === this.state.scannedItem.type &&
        data === this.state.scannedItem.data) ||
      data === null
    ) {
      return;
    }

    Vibration.vibrate();
    this.setState({ scannedItem: { data, type } });
    console.log(this.scannedItem);
  }

  // Render Additional Screen Message
  renderMessage() {
    if (this.state.scannedItem && this.state.scannedItem.type) {
      const { type, data } = this.state.scannedItem;
      return (
        <Text style={styles.scanScreenMessage}>
          {this.showScreenMessage(data)}
        </Text>
      );
    }
    return (
      <Text style={styles.scanScreenMessage}>Focus the barcode to scan.</Text>
    );
  }

  // Reset states
  resetScanner() {
    this.scannedCode = null;
    this.setState({
      scannedItem: {
        type: null,
        data: null,
      },
    });
  }

  // Alert user before adding a contact
  showScreenMessage = (data) => {
    Alert.alert(
      "Are you sure you want to add this contact?",
      `Username: \n${data}`,
      [
        {
          text: "Cancel",
          onPress: () => this.resetScanner(),
        },
        {
          text: "Confirm",
          onPress: () => this.handleScan(data),
        },
      ],
      { cancelable: false }
    );
  };

  // Validating the Scanned Username
  handleScan = (data) => {
    // Check if username exists
    firebase
      .database()
      .ref("users")
      .once("value")
      .then((snapShot) => {
        snapShot.forEach((item) => {
          if (data === item.key) {
            this.state.validUser = true;
          }
        }); // end of for-each

        // Add the user in contacts if it exists
        if (this.state.validUser) {
          this.state.validUser = false; // reset validation
          this.addContact(data); // add the scanned username as a contact
        } else {
          Alert.alert(
            "Unable to found user: " + data + ". \n Please try again."
          );
          console.log(
            "Unable to found user: " + data + ". \n Please try again."
          );
        }
      }); // end of snapshot
  };

  // Adding contact
  addContact = (data) => {
    // Check if the user is already added
    firebase
      .database()
      .ref("/contacts/" + this.state.currentUser)
      .child(data)
      .once("value", (snapshot) => {
        if (snapshot.exists()) {
          Alert.alert("User: " + data + " is already added in your contacts.");
          console.log("User: " + data + " is already added in your contacts.");
        }
        // Else, add the user
        else {
          firebase
            .database()
            .ref("contacts")
            .child(this.state.currentUser)
            .update({
              [data]: true,
            })
            .then((res) => {
              Alert.alert(data + " was successfully added in your contacts.");
              console.log(data + " was successfully added in your contacts.");
            })
            .catch((error) => {
              console.log(error);
            });
        } // end of if-else
      });
    this.resetScanner(); // reset scanner data to allow scanning again
  }; // end of function

  render() {
    // Camera Permission
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <BarCodeScanner
            onBarCodeScanned={this.onBarCodeRead}
            style={StyleSheet.absoluteFill}
          />
          {this.renderMessage()}
        </View>
      </View>
    );
  }
}

// UI Stylesheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
  },
  scanScreenMessage: {
    fontSize: 20,
    color: "white",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default ExpoScanner;
