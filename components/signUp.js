import React from "react";
import { Dropdown } from "react-native-material-dropdown";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from "react-native";
import LoadingScreen from "./loadingScreen";
import "../environment/config";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";

class SignUp extends React.Component {
  
  // States and Parameters
  state = {
    username: "",
    firstName: "",
    lastName: "",
    password: "",
    gender: "",
    isLoading: false,
    exists: false,
  };

  // Validating User Details
  handleSignUp = () => {
    // Check if the fields are complete
    if (
      this.state.username === "" ||
      this.state.firstName === "" ||
      this.state.lastName === "" ||
      this.state.gender === "" ||
      this.state.password === ""
    ) {
      Alert.alert("Please provide complete details to register.");
    } else {
      // Render Loading Screen
      this.setState({ isLoading: true });

      // Check if the username already exists
      firebase
        .database()
        .ref("users")
        .once("value")
        .then((snapShot) => {
          snapShot.forEach((item) => {
            if (item.key === this.state.username) {
              this.state.exists = true;
            }
          }); // end of for-each

          // Alert the user if username already exists
          if (this.state.exists === true) {
            this.setState({
              isLoading: false,
              exists: false,
            });
            Alert.alert("Username already exists.");
            console.log("Username already exists");
          }
          // Otherwise, register account
          else {
            this.addData();
          }
        }); // end of snapshot
    } // end of if-else
  }; // end of function

  // Adding Data
  addData = () => {
    firebase
      .database()
      .ref("users")
      .child(this.state.username)
      .set({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        gender: this.state.gender,
        password: this.state.password,
      })
      .then(() => {
        Alert.alert("User registered successfully.");
        console.log("User registered successfully.");

        // Reset States
        this.setState({
          username: "",
          firstName: "",
          lastName: "",
          gender: "",
          password: "",
          isLoading: false,
          exists: false,
        });
        this.props.navigation.navigate("Login");
      })
      .catch((error) => {
        console.log(error);
      });
  }; // end of function

  render() {
    // Show Loading Screen if 'true'
    if (this.state.isLoading) {
      return <LoadingScreen />;
    }

    // Initialize Dropdown Gender Values
    let data = [{ value: "Male" }, { value: "Female" }];

    return (
      <View style={styles.container}>
        <Text style={styles.createAccountText}>Create Your Account</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Username"
          onChangeText={(username) => this.setState({ username })}
          value={this.state.username}
        />
        <TextInput
          style={styles.textInput}
          placeholder="First Name"
          onChangeText={(firstName) => this.setState({ firstName })}
          value={this.state.firstName}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Last Name"
          onChangeText={(lastName) => this.setState({ lastName })}
          value={this.state.lastName}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Password"
          secureTextEntry
          onChangeText={(password) => this.setState({ password })}
          value={this.state.password}
        />
        <Dropdown
          dropdownposition={1}
          label="Select Gender"
          data={data}
          containerStyle={{
            width: 150,
          }}
          onChangeText={(gender) => this.setState({ gender })}
          value={this.state.gender}
        />
        <TouchableOpacity
          style={styles.btnSignUp}
          onPress={() => this.handleSignUp()}
        >
          <Text style={styles.signUpText}>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

// UI Stylesheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  textInput: {
    backgroundColor: "#eeeeee",
    height: 40,
    width: "70%",
    paddingHorizontal: 10,
    color: "#333",
    marginBottom: 10,
  },
  createAccountText: {
    marginBottom: 30,
    fontSize: 20,
    fontWeight: "bold",
    color: "#616161",
  },
  btnSignUp: {
    backgroundColor: "#8e8e8e",
    paddingHorizontal: 103,
    paddingVertical: 10,
    borderRadius: 5,
  },
  signUpText: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 15,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
  },
});

export default SignUp;