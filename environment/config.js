import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyD0Y64qEFcDEGqFYDZDzl8Eu9hhQIMeiyM",
  authDomain: "mobiletestapp-e0dc0.firebaseapp.com",
  databaseURL: "https://mobiletestapp-e0dc0.firebaseio.com",
  projectId: "mobiletestapp-e0dc0",
  storageBucket: "mobiletestapp-e0dc0.appspot.com",
  messagingSenderId: "1086421226209",
  appId: "1:1086421226209:web:311139506f93ea9f58dda0"
};
// Initialize Firebase
// firebase.initializeApp(firebaseConfig);

export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();